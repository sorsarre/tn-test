#include "sqsolve/result_printer.hpp"
#include <cmath>

//------------------------------------------------------------------------------
sqsolve::ostream_result_printer::ostream_result_printer(std::ostream &out): _out(out)
{

}

//------------------------------------------------------------------------------
void sqsolve::ostream_result_printer::print(
        number_t a,
        number_t b,
        number_t c,
        result_printer::result_t r)
{
    _out << "(" << a << " " << b << " " << c << ") => ";
    if (std::isnan(r.first) && std::isnan(r.second)) {
        _out << "no roots\n";
    } else if (std::isnan(r.second)) {
        _out << "(" << r.first << ")\n";
    } else {
        _out << "(" << r.first << ", " << r.second << ")\n";
    }
}