#include "sqsolve/solve.hpp"
#include "sqsolve/solver.hpp"

//------------------------------------------------------------------------------
sqsolve::simple_solver::simple_solver(std::shared_ptr<sqsolve::result_printer> printer)
        : _printer(printer)
{

}

//------------------------------------------------------------------------------
void sqsolve::simple_solver::solve(number_t a, number_t b, number_t c)
{
    auto roots = sqsolve::solve(a, b, c);
    _printer->print(a, b, c, roots);
}