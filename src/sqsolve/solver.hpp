#pragma once
#include <memory>
#include "sqsolve/common.hpp"
#include "sqsolve/result_printer.hpp"

namespace sqsolve
{
    class simple_solver
    {
    public:
        simple_solver(std::shared_ptr<sqsolve::result_printer> printer);
        void solve(number_t a, number_t b, number_t c);

    private:
        std::shared_ptr<sqsolve::result_printer> _printer;
    };
}