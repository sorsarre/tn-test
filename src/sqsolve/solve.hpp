#pragma once
#include <utility>
#include <limits>
#include <cmath>

namespace sqsolve
{
    template<typename Number>
    std::pair<Number, Number> solve(Number a, Number b, Number c)
    {
        std::pair<Number, Number> ret;

        if (a == 0 && b != 0) { // Not really a quadratic equation, but anyway...
            ret.first = -c / b;
            ret.second = std::numeric_limits<Number>::quiet_NaN();
        } else {
            Number det = std::sqrt(b*b - 4*a*c);
            Number div = 2*a;

            if (det == 0) { // Single root
                ret.first = -b / div;
                ret.second = std::numeric_limits<Number>::quiet_NaN();
            } else { // Two distinct roots
                ret.first = (-b + det) / div;
                ret.second = (-b - det) / div;
            }
        }

        return ret;
    };
}