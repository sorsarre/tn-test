#pragma once
#include <ostream>
#include "sqsolve/common.hpp"

namespace sqsolve
{
    //--------------------------------------------------------------------------
    class result_printer
    {
    public:
        typedef std::pair<number_t, number_t> result_t;

        virtual void print(number_t a, number_t b, number_t c, result_t r) = 0;
        virtual ~result_printer() = default;
    };

    //--------------------------------------------------------------------------
    class ostream_result_printer: public result_printer
    {
    public:
        ostream_result_printer(std::ostream& out);
        void print(number_t a, number_t b, number_t c, result_printer::result_t r) override;

    private:
        std::ostream& _out;
    };
}