#pragma once
#include <istream>
#include <type_traits>
#include <memory>

#include "common.hpp"

namespace sqsolve
{
    //--------------------------------------------------------------------------
    class input
    {
    public:
        virtual bool done() const = 0;
        virtual number_t next() = 0;

        static std::shared_ptr<input> create(int argc, char** argv, std::istream& in);

        virtual ~input() = default;
    };

    //--------------------------------------------------------------------------
    class stream_input: public input
    {
    public:
        stream_input(std::istream& in);

        bool done() const override;
        number_t next() override;

        ~stream_input();
    private:
        void next_internal();
        struct impl;
        std::unique_ptr<impl> _impl;
    };

    //--------------------------------------------------------------------------
    class args_input: public input
    {
    public:
        args_input(size_t argc, char const* const* argv);
        bool done() const override;
        number_t next() override;

        ~args_input();
    private:
        void next_internal();

        struct impl;
        std::unique_ptr<impl> _impl;
    };
}