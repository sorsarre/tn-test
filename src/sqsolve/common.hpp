#pragma once
#include <string>
#include <type_traits>

namespace sqsolve
{
    typedef float number_t;

    template<typename Number>
    Number read_float(const std::string& token, std::size_t& pos)
    {
        static_assert(std::is_floating_point<Number>::value, "Template is only avilable for floating point types");
        pos = 0;
        return 0;
    }

    template<> float read_float<float>(const std::string& token, std::size_t& pos);
    template<> double read_float<double>(const std::string& token, std::size_t& pos);
    template<> long double read_float<long double>(const std::string& token, std::size_t& pos);
}