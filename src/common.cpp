#include <string>
#include "sqsolve/common.hpp"

namespace sqsolve {
    template<>
    float read_float<float>(const std::string &token, std::size_t &pos) {
        return std::stof(token, &pos);
    }

    template<>
    double read_float<double>(const std::string &token, std::size_t &pos) {
        return std::stod(token, &pos);
    }

    template<>
    long double read_float<long double>(const std::string &token, std::size_t &pos) {
        return std::stold(token, &pos);
    }
}