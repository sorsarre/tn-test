#include <memory>
#include <iostream>
#include "sqsolve/common.hpp"
#include "sqsolve/input.hpp"
#include "sqsolve/result_printer.hpp"
#include "sqsolve/solver.hpp"

class Application
{
public:
    int run(int argc, char** argv)
    {
        auto input = sqsolve::input::create(argc, argv, std::cin);
        auto result_printer = std::make_shared<sqsolve::ostream_result_printer>(std::cout);
        sqsolve::simple_solver solver(result_printer);

        sqsolve::number_t a, b, c;
        try {
            while (!input->done()) {
                a = input->next();
                b = input->next();
                c = input->next();

                solver.solve(a, b, c);
            }
        } catch(const std::exception& e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }

        return 0;
    }
};

int main(int argc, char** argv)
{
    Application a;
    return a.run(argc, argv);
}