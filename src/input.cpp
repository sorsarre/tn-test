#include "sqsolve/input.hpp"
#include <iterator>
#include <cstring>

namespace sqsolve
{
    //--------------------------------------------------------------------------
    std::shared_ptr<input> input::create(int argc, char **argv, std::istream &in)
    {
        if (argc > 1) { // prefer args if present
            return std::make_shared<sqsolve::args_input>(argc, argv);
        } else {
            return std::make_shared<sqsolve::stream_input>(in);
        }
    }

    //--------------------------------------------------------------------------
    struct stream_input::impl {
        std::istream& _in;
        std::string _token;
        number_t _last;
        bool _done;

        impl(std::istream& in): _in(in), _done(false)
        {

        }
    };

    //--------------------------------------------------------------------------
    stream_input::stream_input(std::istream& in): _impl(new impl(in))
    {
        next_internal();
    }

    //--------------------------------------------------------------------------
    bool stream_input::done() const
    {
        return _impl->_done;
    }

    //--------------------------------------------------------------------------
    number_t stream_input::next()
    {
        if (done()) {
            throw std::range_error("No more valid input provided");
        }

        number_t ret = _impl->_last;
        next_internal();
        return ret;
    }

    //--------------------------------------------------------------------------
    void stream_input::next_internal()
    {
        _impl->_token.clear();
        std::size_t pos;
        while (!+_impl->_in.eof() && !_impl->_token.size()) {
            _impl->_in >> _impl->_token;
        }

        try {
            _impl->_last = read_float<number_t>(_impl->_token, pos);
            if ((pos != _impl->_token.size()) || !_impl->_token.size()) {
                _impl->_done = true;
                return;
            }
        } catch(const std::exception&) {
            _impl->_done = true;
        }
    }

    //--------------------------------------------------------------------------
    stream_input::~stream_input() = default;

    //--------------------------------------------------------------------------
    struct args_input::impl {
        number_t _last;
        std::size_t _argc;
        char const* const* _argv;
        std::size_t _current;
        bool _done;
    };

    //--------------------------------------------------------------------------
    args_input::args_input(size_t argc, char const* const* argv): _impl(new impl())
    {
        _impl->_argc = argc;
        _impl->_argv = argv;
        _impl->_current = 1;
        _impl->_done = false;
        next_internal();
    }

    //--------------------------------------------------------------------------
    bool args_input::done() const
    {
        return _impl->_done;
    }

    //--------------------------------------------------------------------------
    number_t args_input::next()
    {
        if (done()) {
            throw std::range_error("No more valid input provided");
        }

        number_t ret = _impl->_last;
        next_internal();
        return ret;
    }

    //--------------------------------------------------------------------------
    void args_input::next_internal()
    {
        std::size_t pos;
        try {
            while ((_impl->_current < _impl->_argc) && _impl->_argv[_impl->_current]) {
                _impl->_last = read_float<number_t>(_impl->_argv[_impl->_current], pos);
                if (pos != std::strlen(_impl->_argv[_impl->_current])) { // parse failure
                    _impl->_done = true;
                    return;
                } else {
                    ++_impl->_current;
                    return;
                }
            }

            if (_impl->_current == _impl->_argc) {
                _impl->_done = true;
            }
        } catch(const std::exception&) {
            _impl->_done = true;
        }
    }

    //--------------------------------------------------------------------------
    args_input::~args_input() = default;
}