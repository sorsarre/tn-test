### Design notes

---

In its entirety, the processing chain of input data into equation solutions output
 consists of three major components combined into a pipeline:
 input handler, equation solver and result formatter.
 
The rationale behind such decomposition is fairly simple: it allows to change each part
of the processing pipeline (input, solution, output) independently, thus enabling us to
add support for new input formats, new output formats or more efficient solvers.

#### Input handler

The first component is input handler. It has the following responsibilities:
* Extraction of coefficients from the input;
* Indication of whether the valid input stream was depleted (no more numbers to read
    garbage encountered);
* Raising an error in case the attempt to fetch a number when none are available.
 
 This component has two implementations — one reads the coefficients from the
 specified input stream, another extracts them from command line arguments.
 Which implementation is used upon the program's invocation is determined at runtime,
 depending on whether the command line arguments are present: if there are command
 line arguments, then the latter implementation is used, the former otherwise.
  
#### Solver

Solver component does the actual math of solving the equations using coefficients
retrieved using the input handler. When the results are ready for output, the solver
feeds them into the result formatter it has to be supplied with upon creation.

There is currently only a simple solver implementation that receives a set of
coefficients (a, b, c), finds roots and immediately passes them down to result formatter.
However, there can be other implementations that require accumulating several
coefficient sets before calculating a solution (e.g. ones based on SIMD instructions)
and passing a batch of results to the result formatter.

#### Result formatter

This one is simple — it creates the actual output from the coefficient sets and
their corresponding solutions.

Currently, there is only one result formatter implementation that prints the results
in a plain text format using the specified output stream object.