### sqsolve

---

#### Overview

sqsolve is a small application intended for solving quadratic equations
(ax^2 + bx + c = 0).

Coefficients for each equation are supplied either via command line
 arguments or via standard input.

The output consists of multiple lines, one for each set of coefficients,
of the following form:

```
(a b c) => (r1, r2) # In case there are two distinct real roots
(a b c) => (r)      # In case there is only one real root
(a b c) => no roots # In case there are no real roots
```

#### Usage

Here are some usage examples:
```
> ./sqsolve 1 0 -1
(1 0 -1) => (1, -1)

> ./sqsolve 1 2 3
(1 2 3) => no roots

> cat data.txt
1 0 -1
1 -2 1
1 2 3
> ./sqsolve < data.txt
(1 0 -1) => (1, -1)
(1 -2 1) => (1)
(1 2 3) => no roots
```

#### Invalid input handling

In cases where the input doesn't contain exactly triplets
(i.e. there is one or two trailing) coefficients or it contains
symbols that do not parse as numbers (1asd boo5%, for example),
the sqsolve parses as many triplets as it can, stopping upon non-parsable
tokens and ignoring any input beyond that point.

#### Building from sources

sqsolve uses CMake for build purposes, so it really does boil down to invoking
cmake with the generator appropriate for your build tool of choice and launching
that tool afterwards.

##### Makefiles

```
> cd build_dir
> cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release sqsolve_dir
> make -j8
```

#### Microsoft Visual Studio

First, run the CMake to generate the solution and project files:
```
> cd build_dir
> cmake -G "Visual Studio 14 2015" sqsolve_dir
```
After that the generated solution can be opened and built in MSVS as usual.