#include "gtest/gtest.h"
#include "sqsolve/solve.hpp"

TEST(SolveTest, AllZeros) {
    auto result = sqsolve::solve<float>(0.0, 0.0, 0.0);
    EXPECT_TRUE(std::isnan(result.first));
    EXPECT_TRUE(std::isnan(result.second));
}

TEST(SolveTest, LastNonZero) {
    auto result = sqsolve::solve<float>(0.0, 0.0, 1.5);
    EXPECT_TRUE(std::isnan(result.first));
    EXPECT_TRUE(std::isnan(result.second));
}

TEST(SolveTest, Linear) {
    auto result = sqsolve::solve<float>(0.0, 2.0, 3.0);
    EXPECT_FLOAT_EQ(result.first, -3.0/2.0);
    EXPECT_TRUE(std::isnan(result.second));
}

TEST(SolveTest, SingleRoot) {
    auto result = sqsolve::solve<float>(1.0, -2.0, 1.0);
    EXPECT_FLOAT_EQ(result.first, 1.0);
    EXPECT_TRUE(std::isnan(result.second));
}

TEST(SolveTest, DistinctRoots) {
    auto result = sqsolve::solve<float>(1.0, 0.0, -1.0);
    EXPECT_FLOAT_EQ(result.first, 1.0);
    EXPECT_FLOAT_EQ(result.second, -1.0);
}

TEST(SolveTest, NoRoots) {
    auto result = sqsolve::solve<float>(1.0, 0.0, 1.0);
    EXPECT_TRUE(std::isnan(result.first));
    EXPECT_TRUE(std::isnan(result.second));
}
