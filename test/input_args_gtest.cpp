#include "gtest/gtest.h"
#include "sqsolve/input.hpp"

TEST(ArgsInput, NoArgs) {
    const char* args[] = { "" };
    sqsolve::args_input input(1, args);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, EmptyArgs) {
    const char* args[] = { "", "", "", "", "" };
    sqsolve::args_input input(5, args);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, SpaceArgs) {
    const char* args[] = { "", "", "\r\n\t", "     ", "    " };
    sqsolve::args_input input(5, args);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, NoNumbersInput) {
    const char* args[] = { "", "abg", "\r\n\t z ", "rety ", " aa" };
    sqsolve::args_input input(5, args);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, GarbageFirst) {
    const char* args[] = { "", "abg", "5.3", "rety ", " aa" };
    sqsolve::args_input input(5, args);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, GarbageMiddle) {
    const char* args[] = { "", "5.34", "bga", "2.134", "3.1415" };
    sqsolve::args_input input(5, args);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 5.34);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, GarbageAfter) {
    const char* args[] = { "", "5.34", "bga", "rety ", " aa" };
    sqsolve::args_input input(5, args);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 5.34);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, SingleNumber) {
    const char* args[] = { "", "1.23456" };
    sqsolve::args_input input(2, args);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 1.23456);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, SingleNumberWithSpaces) {
    const char* args[] = { "", "\r\n\t  1.23456  \n" };
    sqsolve::args_input input(2, args);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 1.23456);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(ArgsInput, MultipleNumbers) {
    const char* args[] = { "", "1.23", "4.56", "7.89" };
    sqsolve::args_input input(4, args);

    ASSERT_FALSE(input.done());
    float value;

    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 1.23);

    ASSERT_FALSE(input.done());
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 4.56);

    ASSERT_FALSE(input.done());
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 7.89);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}