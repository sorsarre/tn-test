#include "gtest/gtest.h"
#include "sqsolve/input.hpp"

#include <sstream>

TEST(StreamInput, EmptyInput) {
    std::istringstream iss("");
    sqsolve::stream_input input(iss);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, SpacesInput) {
    std::istringstream iss("\r\n\t ");
    sqsolve::stream_input input(iss);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, NoNumbersInput) {
    std::istringstream iss(" adsgb  ");
    sqsolve::stream_input input(iss);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, GarbageFirst) {
    std::istringstream iss("1adsgb 3.1415926");
    sqsolve::stream_input input(iss);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, GarbageMiddle) {
    std::istringstream iss("3.1415926 1asdf 2.75");
    sqsolve::stream_input input(iss);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 3.1415926);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, GarbageAfter) {
    std::istringstream iss("3.1415926 1asdf");
    sqsolve::stream_input input(iss);

    ASSERT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 3.1415926);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, SingleNumber) {
    std::istringstream iss("123.456");
    sqsolve::stream_input input(iss);

    EXPECT_FALSE(input.done());
    float value;
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 123.456);
    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, SingleNumberWithSpaces) {
    std::istringstream iss("\r   \t\n123.456  \t\r\n");
    sqsolve::stream_input input(iss);

    EXPECT_FALSE(input.done());
    float value;
    EXPECT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 123.456);
    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

TEST(StreamInput, MultipleNumbers) {
    std::istringstream iss(" 1.23 4.56 7.89 ");
    sqsolve::stream_input input(iss);

    ASSERT_FALSE(input.done());
    float value;

    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 1.23);

    ASSERT_FALSE(input.done());
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 4.56);

    ASSERT_FALSE(input.done());
    ASSERT_NO_THROW(value = input.next());
    EXPECT_FLOAT_EQ(value, 7.89);

    EXPECT_TRUE(input.done());
    EXPECT_THROW(input.next(), std::range_error);
}

